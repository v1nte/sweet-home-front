import { createStore } from 'vuex'
import axios from 'axios'
import router from '@/router'

export default createStore({
  state: {
    Token: '',
    User: ''
  },
  mutations: {
    setToken(state, Token){
      state.Token = Token
    },
    clearToken(state){
      state.Token = ''
    },
    setUser(state, User){
      state.User = User
    },
    clearUser(state){
      state.User = ''
    },
  },
  actions: {
    login({ commit }, loginData) {
      axios.post('http://localhost:8000/api/login',loginData, {
        headers: { 'Content-Type': 'application/json' }
      })
        .then(res => {
          console.log(loginData, res.data)
          return res.data
        })
        .then(data => { 
          commit('setToken', data.Token)
          commit('setUser', data.name)
          router.push('/')
        })
        .catch(err => console.error(err))

    }
  },
  modules: {
  }
})
